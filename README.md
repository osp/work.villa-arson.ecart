# Incertains genres

<http://incertains-genres.reseau-ecart.eu>

Website for the 2015 publication of Réseau ECART (European Ceramic Art & Research Team).

![](http://gitlab.constantvzw.org/osp/work-villa-arson-ecart/raw/master/iceberg/cover.png)

Featuring works from ceramic workshops, lectures of schools part of the network: 
    - La Cambre (Brussels), 
    - Pavillon Bosio (Monaco), 
    - Villa Arson (Nice), 
    - Esba (Le Mans) 
    - Ensa (Limoges). 

The network made an exhibition at Keramis <http://www.keramis.be>.

We designed the website of the ceramic network which is also the catalogue of their 2015 exhibition <http://incertains-genres.reseau-ecart.eu> under an invitation by Céline Chazalviel from ENSA Villa Arson (Nice).


## GRAPHS
The website relies mostly on graphs linking each person/location. We used GraphViz <http://graphviz.org>, our favorite graph tool, for this. The relationships from one graph to another are always the same, but the active node is always on the top left corner of the page and thus makes the graph rearrange accordingly.


<img src="http://gitlab.constantvzw.org/osp/work-villa-arson-ecart/raw/master/iceberg/ECOL_Villa_Arson.png" style="width: 50%; float: left;" />
<img src="http://gitlab.constantvzw.org/osp/work-villa-arson-ecart/raw/master/iceberg/Des_genres_au_queer.png" style="width: 50%; float: left;" />

### GENERATE THE GRAPHVIZ
To generate all graphs, open a terminal and, assuming you're already in the project's directory, do:

    cd graphviz
    bash make-graphs.sh

## FONTS

We used the family of Metafonts Tsukurimashou by Matthew Skala. It is a set of fonts made for Japanese typesetting for two pedagogical purposes: learn Japanese and learn type design. 
Because this is a parametrized design built with Metafont, users can generate a potentially unlimited number of typeface designs from the source code; but several ready-made faces are already provided parameters for, each of which can be drawn in monospace or proportional forms.

- Font page: <http://tsukurimashou.osdn.jp/tsukurimashou.pdf>
- Type specimen: <http://rwthaachen.dl.osdn.jp/tsukurimashou/58285/demo.pdf>
- User manual: <http://tsukurimashou.osdn.jp/tsukurimashou.pdf>

![](http://gitlab.constantvzw.org/osp/work-villa-arson-ecart/raw/master/iceberg/tsukurimashou-metafont.png)

From this meta-family we used :

- Tsukurimashou Mincho
- Tsukurimashou Maru  
- Tsukurimashou Anbiruteki PS

We designed the Tsukurimashou XX PS in modifying version of the Matthew Skala's version to have a slighly less bold font and have more clean, precise and readable latin letters.

You can find extra info on how to parameter a Tsukurimashou font in the correspondance with Matthew Skala available in the file `Tsukurimashou.md` (published when we'll have Matthew Skala's agreement).



## ETHERTOFF

The website has been created with Ethertoff, a simple collaborative web platform, a wiki featuring realtime editing thanks to Etherpad. Its output is constructed with equal love for print and web. 

![](http://gitlab.constantvzw.org/osp/work-villa-arson-ecart/raw/master/iceberg/etherpad.png)



## SOURCES

You are free to reuse, modify, redistribute our work under the terms of the Free Art License and the Creative Commons-Attribution-ShareAlike (CC-BY-SA).

- Website: <http://gitlab.constantvzw.org/osp/tools-ethertoff/tree/villa-arson>
- Mock-up and graphs : <http://osp.kitchen/work/villa-arson.ecart/>

